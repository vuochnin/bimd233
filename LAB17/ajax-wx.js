// weather update button click
$('button').on('click', function(e) {
  $('ul li').each(function() {
    console.log("this:" + this);
    $(this).remove();
  });
  $.ajax({
    url: "http://api.wunderground.com/api/ec5317dee0fa5613/geolookup/conditions/q/WA/Bothell.json",
    dataType: "jsonp",
    success: function(parsed_json) {
      var city = parsed_json['location']['city'];
      var state = parsed_json['location']['state'];
      var temp_f = parsed_json['current_observation']['temp_f'];
      var rh = parsed_json['current_observation']['relative_humidity'];
	  var weather = parsed_json['current_observation']['weather'];
	  var wind = parsed_json['current_observation']['wind_string'];
      var str = "<li> <strong>Location : </strong>" + city + ", " + state + "</li>";
	  
	  var fileloc = parsed_json['current_observation']['icon_url'];
	  var icon = "<img src='" + fileloc + "' width = '25'>";
	  
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> <strong>Current temperature is: </strong>" + temp_f + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> <strong>Relative Humidity is: </strong>" + rh + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
	  var str = "<li> <strong>Current weather is: </strong>" + weather + " " + icon + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
	  var str = "<li> <strong>Current wind is: </strong>" + wind + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      // console.log("Current temperature in " + location + " is: " + temp_f);
	  
    }
  });
});

//toLowerCase
//replace