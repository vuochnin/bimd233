function Flight(airline, number, type, origin, destination, dep_time, arrival_time, arrival_gate){
	this.airline = airline;
	this.number = number;
	this.type = type;
	this.origin = origin;
	this.destination = destination;
	this.dep_time = dep_time;
	this.arrival_time = arrival_time;
	this.arrival_gate = arrival_gate;
	
	this.getDuration = (function(){
		var t1 = new Date(dep_time);
		var t2 = new Date(arrival_time);
		var timeInMs = t2 - t1;
		var msPerHour = 1000 * 60 * 60;
		var timeInHour = timeInMs / msPerHour;
		return timeInHour.toFixed(2);
	}());
}

var flight1 = new Flight('Alaska', 'AK333', 'B737', 'KSEA', 'KGEG', 'May 5, 2016 18:00:00', 'May 5, 2016 19:20:00', 'C7');
var flight2 = new Flight('United', 'U2001', 'A320', 'KORD', 'NYC', 'May 5, 2016 19:15:00', 'May 5, 2016 22:10:00', 'N17');
var flight3 = new Flight('Delta', 'DAL', 'MD90', 'KORD', 'KNYC', 'May 5, 2016 19:15:00', 'May 5, 2016 22:10:00', 'N17');
var flight4 = new Flight('Delta', 'DAL', 'A333', 'KATL', 'MAD', 'May 10, 2016 18:02:00', 'May 11, 2016 08:14:00', 'N18')
var flight5 = new Flight('Alaska', 'AK333', 'B737', 'EZE', 'KMSP', 'May 10, 2016 18:30:00', 'May 11, 2016 19:20:00', 'C8');

var flights = [flight1, flight2, flight3, flight4, flight5];

var el = document.getElementById("table_head");
el.innerHTML = "<tr><th>Airline Name</th> <th>Number</th> <th>Type</th> <th>Origin</th> <th>Destination</th> <th>Departure Time</th> <th>Arrival Time</th> <th>Arrival Gate</th> <th>Duration</th></tr>"

var i = 0;
el = document.getElementById("table_data");
el.innerHTML = "<tr><td>" + flights[i].airline + "</td><td>" + flights[i].number + "</td><td>" + flights[i].type + "</td><td>" + flights[i].origin + "</td><td>" + flights[i].destination + "</td><td>" + flights[i].dep_time + "</td><td>" + flights[i].arrival_time + "</td><td>" + flights[i].arrival_gate + "</td><td>" + flights[i].getDuration + "</td></tr>";

var i = 1;
el.innerHTML += "<tr><td>" + flights[i].airline + "</td><td>" + flights[i].number + "</td><td>" + flights[i].type + "</td><td>" + flights[i].origin + "</td><td>" + flights[i].destination + "</td><td>" + flights[i].dep_time + "</td><td>" + flights[i].arrival_time + "</td><td>" + flights[i].arrival_gate + "</td><td>" + flights[i].getDuration + "</td></tr>";

var i = 2;
el.innerHTML += "<tr><td>" + flights[i].airline + "</td><td>" + flights[i].number + "</td><td>" + flights[i].type + "</td><td>" + flights[i].origin + "</td><td>" + flights[i].destination + "</td><td>" + flights[i].dep_time + "</td><td>" + flights[i].arrival_time + "</td><td>" + flights[i].arrival_gate + "</td><td>" + flights[i].getDuration + "</td></tr>";

var i = 3;
el.innerHTML += "<tr><td>" + flights[i].airline + "</td><td>" + flights[i].number + "</td><td>" + flights[i].type + "</td><td>" + flights[i].origin + "</td><td>" + flights[i].destination + "</td><td>" + flights[i].dep_time + "</td><td>" + flights[i].arrival_time + "</td><td>" + flights[i].arrival_gate + "</td><td>" + flights[i].getDuration + "</td></tr>";

var i = 4;
el.innerHTML += "<tr><td>" + flights[i].airline + "</td><td>" + flights[i].number + "</td><td>" + flights[i].type + "</td><td>" + flights[i].origin + "</td><td>" + flights[i].destination + "</td><td>" + flights[i].dep_time + "</td><td>" + flights[i].arrival_time + "</td><td>" + flights[i].arrival_gate + "</td><td>" + flights[i].getDuration + "</td></tr>";



