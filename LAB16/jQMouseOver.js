
$('input').keypress(function(e) {
	var code = e.which;
	if(code == 13){
		code = this.value;
		$('ul').prepend("<li class='list-group-item'>" + code + "</li>");
		//$('li:first').attr('id', 'deselected');
		$('input').val("");	// clear the input value after inserted to the list
		handleMouseOver();
		handleMouseLeave();
	}
});

function handleMouseOver(){
	$('li').on("mouseover", function() {
		$(this).attr('id', 'selected');
		//$(this).text('Focused!');
	});	
}

function handleMouseLeave(){
	$('li').on("mouseleave", function() {
		$(this).attr('id', 'deselected');
		//$(this).text('Not focused');
	});
}
handleMouseOver();
handleMouseLeave();


