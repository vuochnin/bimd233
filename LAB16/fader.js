$('li').css('margin','10px');
$('li').attr('id','uw');

// Fade-Out
$('#p1 li').click(function() {
  console.log("$(this):" + $(this));
    $(this).fadeOut( 2000, function() {
  });
});

// Fade-In
var temp = document.getElementById('showFadeIn');
var prevText = temp.innerHTML;
var p2Lists = document.querySelectorAll('#p2 li');
for(var i = 0; i < p2Lists.length; i++){
	$(p2Lists[i]).fadeOut( 1000 * i, function() {
		temp.innerHTML = "Click Here!";
	});
}

$('#showFadeIn').click(function() {
	for(var i = 0; i < p2Lists.length; i++){
		temp.innerHTML = prevText;
		$(p2Lists[i]).fadeIn( 2000 * i);
	}
  
});


// Fade-To
$('#p3 li').click(function() {
  console.log("$(this):" + $(this));
    // $(this).fadeTo( 2000, function() {
		// console.log("fadeto complete!");
		// $(this).css('text-align', 'center');
	// });
	$(this).fadeTo(2000, 0.5);
});

// Fade-Toggle
var p4 = document.getElementById('doFadeToggle');
p4.innerHTML = "Click/Unclick for fadeToggle";
var lists = document.querySelectorAll('#p4 li');
$('#doFadeToggle').click(function() {
  console.log("$(this):" + $(this));
    $(lists[0]).fadeToggle(2000);
	$(lists[1]).fadeToggle("fast");
	$(lists[2]).fadeToggle();
});