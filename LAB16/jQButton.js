$('button').click(function() {
  console.log("=>this:" + this + "\t=>$(this):" + $(this));
  console.log("=>data: " + $(this).text());
});

$('input').keypress(function(e) {
	var code = e.which;
	var char = String.fromCharCode(code);
	console.log("\nKey Code: " + code + "\tCharacter: "+ char);
});

// $('button').click(function() {
//   var color = $(this).css("background");
//   console.log("=>this:" + this + "-|-$(this):");
//   if ($(this).css("background") === "red") {
//     $(this).css("background", "blue");
//   } else {
//     if ($(this).css("background") === "blue") {
//       $(this).css("background", "");
//     } else {
//       $(this).css("background", "red");
//     }
//   }
// });