var name = prompt("What's the name of your car?");
var date = prompt("What's the date?");
var miles = prompt("What's the mileage driven?");

var carName = document.getElementById('car-name');
carName.textContent = "Car name: " + name;

var dt = document.getElementById('date');
dt.textContent = "Date: " + date;

var mi = document.getElementById('miles');
mi.textContent = "Miles driven: " + miles;

var res = document.getElementById('result');
res.textContent = "Deduction value: " + (miles * 0.57).toFixed(2) + " dollars";