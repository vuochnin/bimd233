function calcCircleGeometries(radius){
	const pi = Math.PI;
	var area = pi * radius * radius;
	var circumference = 2 * pi * radius;
	var diameter = 2 * radius;
	var geometries = [area, circumference, diameter];
	return geometries;
}

for(var i=1; i<4; i++){
	var rad = 20 * i;
	var geoArray = calcCircleGeometries(rad);
	var desc = document.getElementById("desc" + i);
	desc.textContent = "With radius of " + rad;
	
	var ar = document.getElementById("area" + i);
	ar.textContent = "Area = " + geoArray[0];

	var circum = document.getElementById("circum" + i);
	circum.textContent = "Circumference = " + geoArray[1];

	var diam = document.getElementById("diam" + i);
	diam.textContent = "Diameter = " + geoArray[2];
}
